<?php

/**
 * Holmbank Checkout Extension BO Controller.
 */
class ControllerExtensionPaymentHolmbank extends Controller
{
    private $error = array();

    /**
     * Check Mark Character Code.
     */
    const CHECK_MARK_CHAR = '&#10004';

    /**
     * Cross Mark Character Code.
     */
    const CROSS_MARK_CHAR = '&#10060';

    private const ORDER_STATUS_NAME = 'Holmbank payment awaiting';

    /**
     * Executed on Extension install - executes SQL queries.
     * @return void
     */
    public function install()
    {
        // Register hook to add calculator on product page.
        $this->load->model('setting/event');
        $this->model_setting_event->addEvent(
            'holmbank_modify_product_page',
            'catalog/view/product/product/before',
            'extension/payment/holmbank/modifyProductPage',
            1,
            1
        );

        $this->model_setting_event->addEvent(
            'holmbank_before_order_add',
            'catalog/model/checkout/order/addOrder/before',
            'extension/payment/holmbank/eventBeforeOrderAdd',
            1,
            1
        );

        // Create Tables and Insert initial Data.
        $this->load->model('extension/payment/holmbank');
        $this->model_extension_payment_holmbank->install();
        $this->addOrderStatuses();
    }

    /**
     * Executed on Extension uninstall - executes SQL queries.
     * @return void
     */
    public function uninstall()
    {
        // Remove hook for displaying calculator on product page.
        $this->load->model('setting/event');
        $this->model_setting_event->deleteEventByCode('holmbank_modify_product_page');
        $this->model_setting_event->deleteEventByCode('holmbank_before_order_add');

        // Remove Module Related Tables.
        $this->load->model('extension/payment/holmbank');
        $this->model_extension_payment_holmbank->uninstall();
    }

    /**
     * Method is responsible for BO Index Page Loading.
     * @return void
     */
    public function index()
    {
        $this->load->language('extension/payment/holmbank');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('setting/setting');

        $settingValue = $this->model_setting_setting->getSetting('payment_holmbank');
        $data['config_setting'] = $settingValue;

        // Actions
        $data['href_back'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment');
        $data['href_save'] = $this->url->link('extension/payment/holmbank/save', 'user_token=' . $this->session->data['user_token']);
        $data['href_add'] = $this->url->link('extension/payment/holmbank/add', 'user_token=' . $this->session->data['user_token']);

        $this->config->get('payment_holmbank_x_payment_link_key');

        $data['payment_my_response'] = $this->config->get('payment_my_response');
        $data['payment_my_geo_zone_id'] = $this->config->get('payment_my_geo_zone_id');
        $data['is_key_initialized'] = $settingValue['payment_holmbank_x_payment_link_key'] ?? false;

        // Setting Page Header Data
        $data['breadcrumbs'] = [
            array('text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'])),
            array('text' => $this->language->get('text_extensions'),
                'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment')),
            array('text' => $this->language->get('heading_title'),
                'href' => $this->url->link('extension/payment/holmbank', 'user_token=' . $this->session->data['user_token']))
        ];
        $data['action'] = $this->url->link('extension/payment/holmbank', 'user_token=' . $this->session->data['user_token'], true);

        // Setting Merchant Table Data
        $data['merchant_product_table_headers'] = [
            $this->language->get('table_id_column_title'),
            $this->language->get('table_id_column_type'),
            $this->language->get('table_id_column_name'),
            $this->language->get('table_id_column_description'),
            $this->language->get('table_id_column_active'),
            $this->language->get('table_id_column_is_calculator_displayed'),
            $this->language->get('table_id_column_interest_rate'),
            $this->language->get('table_id_column_payment_period'),
            $this->language->get('table_id_column_calculator_text'),
            $this->language->get('table_id_column_calculator_link'),
            ''
        ];

        $this->load->model('localisation/order_status');
        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
        $data['selected_deny_order_status'] = $this->config->get('payment_holmbank_denied_order_status');
        $data['selected_accept_order_status'] = $this->config->get('payment_holmbank_accepted_order_status');

        // Get Al Merchant Products Located in DB.
        $this->load->model('extension/payment/holmbank');
        $holmbankRepository = $this->model_extension_payment_holmbank;
        $merchantProducts = $holmbankRepository->getAllMerchantProducts();

        // Remove image path URL from display.
        foreach ($merchantProducts as $key => $merchantProduct)
        {
            unset($merchantProducts[$key]['logo']);

            // Change to icons.
            $merchantProducts[$key]['is_enabled'] = $merchantProducts[$key]['is_enabled'] == 1 ? self::CHECK_MARK_CHAR : self::CROSS_MARK_CHAR;
            $merchantProducts[$key]['is_calculator_displayed'] = $merchantProducts[$key]['is_calculator_displayed'] == 1 ? self::CHECK_MARK_CHAR : self::CROSS_MARK_CHAR;

            $merchantProducts[$key]['editUrl'] = $this->url->link('extension/payment/holmbank/edit'
                , 'user_token=' . $this->session->data['user_token'] . '&id=' . $merchantProducts[$key]['id_merchant_product']);
            $merchantProducts[$key]['deleteUrl'] = $this->url->link('extension/payment/holmbank/delete'
                , 'user_token=' . $this->session->data['user_token'] . '&id=' . $merchantProducts[$key]['id_merchant_product']);
        }

        $data['merchant_product_table_data'] = $merchantProducts;

        // Add Common Components to Extension Settings View.
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/payment/holmbank', $data));
    }

    /**
     * Method handles basic module settings save.
     * @return void
     */
    public function save(): void
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST'))
        {
            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('payment_holmbank', $this->request->post);
            $this->response->redirect($this->url->link('extension/payment/holmbank', 'user_token=' . $this->session->data['user_token']));
        }
    }

    /**
     * Method handles GET and POST request to/from merchant product edit page.
     * @return void
     */
    public function edit(): void
    {
        $this->load->model('extension/payment/holmbank');
        if (($this->request->server['REQUEST_METHOD'] == 'GET'))
        {
            $holmbankRepository = $this->model_extension_payment_holmbank;
            $merchant_product = $holmbankRepository->getMerchantProductById($this->request->get['id']);
            $this->load->model('setting/setting');
            $this->load->language('extension/payment/holmbank');
            $this->document->setTitle($this->language->get('heading_title_main'));

            // API Controller
            $this->load->model('setting/setting');
            $mySettingValue = $this->model_setting_setting->getSetting('payment_holmbank');

            // API Controller
            require_once DIR_SYSTEM .'library/holmbank/holmbank.php';
            $this->load->model('setting/setting');
            $mySettingValue = $this->model_setting_setting->getSetting('payment_holmbank');

            $holmbank_api = new HolmbankApi($mySettingValue);
            $merchant_product_types = $holmbank_api->getLoanProducts();

            $data['merchant_product_data'] = $merchant_product;
            $data['merchant_product_id'] = $this->request->get['id'];
            $data['merchant_product_types'] = $merchant_product_types;
            $data['form_type_label'] = $this->language->get('form_type_label');
            $data['form_name_label'] = $this->language->get('form_name_label');
            $data['form_description_label'] = $this->language->get('form_description_label');
            $data['form_active_label'] = $this->language->get('form_active_label');
            $data['form_is_calculator_displayed_label'] = $this->language->get('form_is_calculator_displayed_label');
            $data['form_interest_rate_label'] = $this->language->get('form_interest_rate_label');
            $data['form_payment_period_label'] = $this->language->get('form_payment_period_label');
            $data['form_calculator_text_label'] = $this->language->get('form_calculator_text_label');
            $data['form_calculator_link_label'] = $this->language->get('form_calculator_link_label');

            // Actions
            $data['href_edit_merchant_product'] = $this->url->link('extension/payment/holmbank/edit', 'user_token=' . $this->session->data['user_token']);

            // Setting Page Header Data
            $data['back'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment');
            $data['breadcrumbs'] = [
                array('text' => $this->language->get('text_home'),
                    'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'])),
                array('text' => $this->language->get('text_extensions'),
                    'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment')),
                array('text' => $this->language->get('heading_title'),
                    'href' => $this->url->link('extension/payment/holmbank', 'user_token=' . $this->session->data['user_token'])),
                array('text' => $this->language->get('merchant_product_add_title'),
                    'href' => $this->url->link('extension/payment/holmbank/edit', 'user_token=' . $this->session->data['user_token']))
            ];
            $data['action'] = $this->url->link('extension/payment/holmbank', 'user_token=' . $this->session->data['user_token'], true);

            // Add Common Components to Extension Settings View.
            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('extension/payment/holmbank_merchantproduct', $data));
        } elseif($this->request->server['REQUEST_METHOD'] == 'POST')
        {
            // Save  Merchant Product to DB.
            $holmbankRepository = $this->model_extension_payment_holmbank;
            $holmbankRepository->updateMerchantProduct($this->request->post);

            $this->index();
        }
    }

    /**
     * Method handles GET and POST request to/from merchant product addition page.
     * @return void
     */
    public function add(): void
    {
        $this->load->model('extension/payment/holmbank');
        if (($this->request->server['REQUEST_METHOD'] == 'GET'))
        {
            $this->load->model('setting/setting');
            $this->load->language('extension/payment/holmbank');
            $this->document->setTitle($this->language->get('heading_title_main'));

            // API Controller
            require_once DIR_SYSTEM . 'library/holmbank/holmbank.php';
            $this->load->model('setting/setting');
            $mySettingValue = $this->model_setting_setting->getSetting('payment_holmbank');

            $holmbank_api = new HolmbankApi($mySettingValue);
            $merchant_product_types = $holmbank_api->getLoanProducts();

            $data['merchant_product_types'] = $merchant_product_types;
            $data['form_type_label'] = $this->language->get('form_type_label');
            $data['form_name_label'] = $this->language->get('form_name_label');
            $data['form_description_label'] = $this->language->get('form_description_label');
            $data['form_active_label'] = $this->language->get('form_active_label');
            $data['form_is_calculator_displayed_label'] = $this->language->get('form_is_calculator_displayed_label');
            $data['form_interest_rate_label'] = $this->language->get('form_interest_rate_label');
            $data['form_payment_period_label'] = $this->language->get('form_payment_period_label');
            $data['form_calculator_text_label'] = $this->language->get('form_calculator_text_label');
            $data['form_calculator_link_label'] = $this->language->get('form_calculator_link_label');
            $data['form_calculator_link_description'] = $this->language->get('form_calculator_link_description');

            // Actions
            $data['href_add_merchant_product'] = $this->url->link('extension/payment/holmbank/add', 'user_token=' . $this->session->data['user_token']);

            // Setting Page Header Data
            $data['back'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment');
            $data['breadcrumbs'] = [
                array('text' => $this->language->get('text_home'),
                    'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'])),
                array('text' => $this->language->get('text_extensions'),
                    'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment')),
                array('text' => $this->language->get('heading_title'),
                    'href' => $this->url->link('extension/payment/holmbank', 'user_token=' . $this->session->data['user_token'])),
                array('text' => $this->language->get('merchant_product_add_title'),
                    'href' => $this->url->link('extension/payment/holmbank/add', 'user_token=' . $this->session->data['user_token']))
            ];
            $data['action'] = $this->url->link('extension/payment/holmbank', 'user_token=' . $this->session->data['user_token'], true);

            // Add Common Components to Extension Settings View.
            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('extension/payment/holmbank_merchantproduct', $data));
        } elseif($this->request->server['REQUEST_METHOD'] == 'POST')
        {
            // Save  Merchant Product to DB.
            $holmbankRepository = $this->model_extension_payment_holmbank;
            $holmbankRepository->addMerchantProduct($this->request->post);

            $this->index();
        }
    }

    /**
     * Method handles GET request to/from merchant product addition page.
     * @return void
     */
    public function delete(): void
    {
        // Delete Merchant Product to DB.
        $this->load->model('extension/payment/holmbank');
        $holmbankRepository = $this->model_extension_payment_holmbank;
        $holmbankRepository->deleteMerchantProduct($this->request->get['id']);

        $this->index();
    }

    /**
     * Method Processes Order Statuses Addition to Database.
     * @return void
     */
    protected function addOrderStatuses()
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE name = '" .
            $this->db->escape(self::ORDER_STATUS_NAME) . "'");

        if ($query->num_rows) {
            // TODO! Potential development place.
        } else {
            // Create Payment Related Order Statuses.
            $this->load->model('localisation/order_status');
            $this->db->query("INSERT INTO " . DB_PREFIX . "order_status SET language_id = '" .
                (int)$this->config->get('config_language_id') . "', name = '" . $this->db->escape(self::ORDER_STATUS_NAME) . "'");
            $this->cache->delete('order_status');
        }
    }
}