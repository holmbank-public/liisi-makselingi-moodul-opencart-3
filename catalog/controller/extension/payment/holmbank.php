<?php

/**
 * Holmbank Checkout Extension Controller.
 */
class ControllerExtensionPaymentHolmbank extends Controller {

    private $error = array();

    /**
     * Holmbank extension every payment method code left part.
     */
    private const PAYMENT_EXTENSION_CODE = 'holmbank';

    /**
     * Holmbank extension awaiting status name.
     */
    private const STATUS_AWAITING = 'HOLMBANK PAYMENT AWAITING';

    /**
     * Method responsible for processing GET requests. Basically displays different merchant products.
     * @return string
     */
    public function index()
    {
        $this->load->language('extension/payment/holmbank');
        $data['href_confirm'] = $this->url->link('extension/payment/holmbank/confirm');

        return $this->load->view('extension/payment/holmbank_confirm', $data);
    }


    /**
     * Method is called on before order add, to change the title.
     */
    public function eventBeforeOrderAdd(&$route, &$data) {
        $this->load->language('extension/payment/holmbank');
        if ($data[0]['payment_code'] == 'holmbank') {
            $data[0]['payment_method'] = $this->language->get('payment_method_display_bo');
        }
    }

    /**
     * Method modify product page and adds calculator to it.
     * @return void
     */
    public function modifyProductPage(&$route, &$data)
    {
        if (isset($this->request->get['product_id'])) {
            $product_id = (int)$this->request->get['product_id'];
        } else {
            $product_id = 0;
        }

        // Check if the extension is enabled
        if (!$this->config->get('payment_holmbank_status')) {
            return;
        }

        $this->load->model('catalog/product');
        $this->load->language('extension/payment/holmbank');

        $product_info = $this->model_catalog_product->getProduct($product_id);

        if ($product_info && isset($product_info['price'])) {
            // Get only active merchant loan products to display to customer.
            $this->load->model('extension/payment/holmbank');
            $holmbankRepository = $this->model_extension_payment_holmbank;
            $merchantLoans = $holmbankRepository->getCalculatorMerchantProducts();

            $product_html = '';
            foreach ($merchantLoans as $merchantDBLoan) {
                // Check if interest rate and payment period are set at all.
                if(empty($merchantDBLoan['interest_rate']) || empty($merchantDBLoan['payment_period'])) {
                    continue;
                }
                $monthlyPayment = number_format((($product_info['price']) *
                        (1 + $merchantDBLoan['interest_rate'] / 100 * $merchantDBLoan['payment_period'] / 12)) / ($merchantDBLoan['payment_period']), 2);

                $merchant_product_data = [
                    'holmbank_payment_logo' => $merchantDBLoan['logo'],
                    'holmbank_payment_logo_alt' => $this->language->get('payment_method_logo'),
                    'holmbank_calculator_estimation' => $monthlyPayment,
                    'holmbank_calculator_min_estimation' => 7,
                    'shop_currency_code' => $this->session->data['currency'],
                    'holmbank_calculator_description' => $merchantDBLoan['calculator_text'],
                    'holmbank_calculator_link' => $merchantDBLoan['calculator_link'],
                    'holmbank_style' => 'catalog/view/theme/default/stylesheet/holmbank/payment.css'
                ];

                $product_html .= $this->load->view('extension/payment/holmbank_product_calculator', $merchant_product_data);
            }

            $data['tax'] = isset($data['tax']) ? $data['tax'] . $product_html : $product_html;
        }
    }

    /**
     * Method responsible for Holmbank payment initialization.
     * @return void
     */
    public function confirm(): void
    {
        if ($this->request->server['REQUEST_METHOD'] == 'POST')
        {
            $this->load->language('extension/payment/holmbank');

            $json = [];

            if (!isset($this->session->data['order_id']))
            {
                $json['error'] = $this->language->get('error_order');
            }

            if (!isset($this->session->data['payment_method']) || $this->session->data['payment_method']['code'] != 'holmbank'
                || !isset($this->request->post['payment_option']))
            {
                $json['error'] = $this->language->get('error_payment_method');
            }

            // Chosen payment is correct, so we can now create order and send request to Holmbank.
            if (!$json)
            {
                $requestBody = $this->assembleRequestBody($this->cart, $this->session->data['order_id']);

                // Cart total should always be equal to sum of products.
                $isValid = $this->orderTotalValidation($requestBody);

                // Totals are not equal to each other, so display error message to the customer and send him back to the cart page.
                if (!$isValid)
                {
                    $this->session->data['error'] = 'Sorry, holmbank can not be initialized. There are problems with your cart!';
                    $this->response->redirect($this->url->link('checkout/cart'));
                }

                // Control that loan product still exist in the backend.
                $isValid = $this->loanPaymentExistValidation($this->request->post['payment_option']);

                // Product was deleted from API system, but still remains in Merchant System. Send customer back to cart.
                if (!$isValid)
                {
                    $this->session->data['error'] = 'Sorry, holmbank can not be initialized. There is a problem with chosen payment method!';
                    $this->response->redirect($this->url->link('checkout/cart'));
                }

                // Order is valid and ready to process API requests.
                $uniqueKey = $this->generateUniqueKey($this->cart);

                // Initialize API Controller Calls - Start Payment Process
                require_once DIR_SYSTEM .'library/holmbank/holmbank.php';
                $this->load->model('setting/setting');
                $mySettingValue = $this->model_setting_setting->getSetting('payment_holmbank');

                $holmbank_api = new HolmbankApi($mySettingValue);
                $response = $holmbank_api->postLoanStart($uniqueKey, $requestBody);

                $response = json_decode($response, true);

                // API returned data is not matched with expectations, so send customer back to the cart.
                if (empty($response["orderLink"]) || empty($response["orderId"]))
                {
                    $this->session->data['error'] = 'Sorry, holmbank can not be initialized. There is a problem with holmbank!';
                    $this->response->redirect($this->url->link('checkout/cart'));
                }

                // Load the necessary models
                $this->load->model('localisation/order_status');
                $this->load->model('checkout/order');

                $paymentData =
                [
                    "id_shop" => $this->config->get('config_store_id'),
                    "id_merchant_order" => $this->session->data['order_id'],
                    "id_holmbank_order" => $response["orderId"],
                    "x_payment_link_req_id" => $uniqueKey,
                ];

                $this->load->model('extension/payment/holmbank');
                $holmbankRepository = $this->model_extension_payment_holmbank;
                $holmbankRepository->addHolmbankOrder($paymentData);

                // Everything is fine. Create Order and Redirect User to start payment and clear cart.
                $this->load->model('checkout/order');
                //$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('payment_free_checkout_order_status_id'));

                // Everything is fine. Redirect User to start payment and clear cart.
                $order_statuses = $this->model_localisation_order_status->getOrderStatuses();

                foreach ($order_statuses as $order_status)
                {
                    if (mb_strtoupper($order_status['name']) == self::STATUS_AWAITING)
                    {
                        $this->model_checkout_order->addOrderHistory(
                            $this->session->data['order_id'],
                            $order_status['order_status_id'],
                            'Holmbank payment',
                            false
                        );
                        break;
                    }
                }

                $this->cart->clear();
                $this->response->redirect($response["orderLink"]);
            }
        }
    }

    /**
     * Method responsible for Holmbank payment return.
     * @return void
     */
    public function return(): void
    {
        if ($this->request->server['REQUEST_METHOD'] == 'POST')
        {
            // Get POST body
            $key = $this->request->server['HTTP_X_PAYMENT_LINK_REQ_ID'];

            $raw = file_get_contents('php://input');
            $data = json_decode($raw);

            // POST comes without any data
            if (empty($data))
            {
                die();
            }

            // Get record from Database that Order exist in Database.
            $this->load->model('extension/payment/holmbank');
            $holmbankRepository = $this->model_extension_payment_holmbank;
            $holmbankOrder = $holmbankRepository->getHolmbankOrderByIdAndKey($data->orderId, $key);

            // Order was found in Database.
            if ($holmbankOrder->num_rows != 0)
            {
                // We need to change Order status based on Bank request data.
                $status = $data->status;
                $this->changeOrderStatus($status, $holmbankOrder->row['id_merchant_order']);
            }
            die();
        }

        $request = $this->request->get;
        if (isset($request['status']) && $request['orderId']) {
            if ($request['status'] == 'REJECTED') {
                $this->response->redirect($this->url->link('checkout/checkout', '', true));
            } else {
                $this->response->redirect($this->url->link('checkout/success', '', true));
            }
        }
    }

    /**
     * Method assemble request body to send it to the API.
     * - All cart products are added to request with final price after applying taxes.
     * - Cart rules are being added as separate products with negative values.
     * - Shipping is also being added as separate product.
     * @param $cart object Defines customer cart.
     * @return array Defines request body.
     */
    private function assembleRequestBody($cart, $idOrder): array
    {
        // NB! orderNumber can not be sent, because it's not created yet.

        // Order Totals
        $taxes = $this->cart->getTaxes();
        $subtotal = $this->cart->getSubTotal();

        $this->load->model('extension/payment/holmbank');
        $holmbankRepository = $this->model_extension_payment_holmbank;
        $paymentMethod = $holmbankRepository->getMerchantProductById($this->request->post['payment_option']);

        $requestBody =
            [
                "language" => $this->language->get('code'),
                "totalAmount" => $subtotal,
                "paymentType" => $paymentMethod['type'],
                "products" => array(),
                "returnUrl" => $this->url->link('extension/payment/holmbank/return')
            ];

        // Assemble body with all chosen by client products.
        $products = $cart->getProducts();
        $productsTotal = 0;

        foreach ($products as $product)
        {
            $productInfo =
                [
                    "productSKU" => $product['product_id'],
                    "productName" => $product['name'],
                    "totalPrice" => $product['total'],
                    "quantity" => $product['quantity'],
                ];
            $requestBody["products"][] = $productInfo;
            $productsTotal += $product['total'];
        }

        // Assemble body with all cart applied vouchers.
        if (!empty($this->session->data['voucher']))
        {
            $this->load->model('extension/total/voucher');
            $voucherInfo = $this->model_extension_total_voucher->getVoucher($this->session->data['voucher']);
            $productInfo =
                [
                    "productName" => 'voucher_' . $voucherInfo['code'],
                    "totalPrice" => -$voucherInfo['amount'],
                    "quantity" => 1,
                ];
            $requestBody["products"][] = $productInfo;
            $requestBody["totalAmount"] -= $voucherInfo['amount'];
        }

        // Assemble body with all cart applied coupons.
        $isShippingFree = false;
        if (!empty($this->session->data['coupon']))
        {
            $this->load->model('extension/total/coupon');
            $couponInfo = $this->model_extension_total_coupon->getCoupon($this->session->data['coupon']);

            // Handle coupon shipping
            $isShippingFree = (bool) $couponInfo['shipping'];

            // Consider that coupon can be with fixed value or percentage 'F' and 'P'
            $couponType = $couponInfo['type'];
            $couponDiscount = $couponInfo['discount'];
            if (0 != $couponDiscount)
            {
                $totalPrice = ('P' === $couponType) ? $productsTotal * $couponDiscount / 100 : $couponDiscount;
                $productInfo =
                    [
                        "productName" => 'coupon_' . $couponInfo['code'],
                        "totalPrice" => -$totalPrice,
                        "quantity" => 1,
                    ];
                $requestBody["products"][] = $productInfo;
                $requestBody["totalAmount"] -= $totalPrice;
            }
        }

        $productInfo =
            [
                "productSKU" => 'Maksud',
                "productName" => 'Maksud',
                "totalPrice" => 0,
                "quantity" => 1,
            ];

        $new_taxes = [];
        foreach ($this->cart->getProducts() as $product) {
            // Calculate the tax based on the new price after applying the coupon discount
            $new_price = ($product['price'] / $subtotal) * (float) $requestBody["totalAmount"];
            $new_tax = $this->tax->calculate($new_price, $product['tax_class_id'], $this->config->get('config_tax'));
            $tax_amount = $product['quantity'] * ($new_tax - $new_price);
            if (empty($new_taxes[$product['tax_class_id']])) {
                $new_taxes[$product['tax_class_id']] = $tax_amount;
            } else {
                $new_taxes[$product['tax_class_id']] += $tax_amount;
            }
        }
        $total_new_tax = array_sum($new_taxes);

        $productInfo["totalPrice"] = $total_new_tax;
        $requestBody["totalAmount"] += $total_new_tax;

        if ($productInfo['totalPrice'] > 0) {
            $requestBody["products"][] = $productInfo;
        }

        // Get shipping Method Cost. NB! coupon can make shipping free.
        if ($this->cart->hasShipping() && !$isShippingFree)
        {
            if (isset($this->session->data['shipping_method']))
            {
                $this->load->model('setting/extension');
                $this->load->model('extension/total/shipping');
                $this->load->model('extension/total/tax');

                $shipping_method = $this->session->data['shipping_method'];
                $shipping_cost = $shipping_method['cost'];

                $shipping_taxes = $this->tax->getRates($shipping_cost, $shipping_method['tax_class_id']);

                $shipping_tax_total = 0;
                foreach ($shipping_taxes as $shipping_tax) {
                    $shipping_tax_total += $shipping_tax['amount'];
                }

                $shipping_total_with_tax = $shipping_cost + $shipping_tax_total;


                $requestBody["products"][] =
                    [
                        "productName" => 'transport',
                        "totalPrice" => $shipping_total_with_tax,
                        "quantity" => 1,
                    ];

                $requestBody['totalAmount'] += $shipping_total_with_tax;
            }
        }

        // Eliminate small rounding differences and round total.
        $productsTotal = 0;
        $requestBody['totalAmount'] = round($requestBody['totalAmount'], 2);

        foreach ($requestBody["products"] as $product)
        {
            $productsTotal += $product['totalPrice'];
        }

        $totalDifference = round($requestBody['totalAmount'] - $productsTotal, 2);
        if (0 != $totalDifference)
        {
            $requestBody["products"][] =
                [
                    "productName" => "total_round_difference",
                    "totalPrice" => $totalDifference,
                    "quantity" => 1,
                ];
        }

        return $requestBody;
    }

    /**
     * Method checks if order matches Holmbank API criteria.
     * - "Cart total amount should always be equal to the products total sum in request"
     * @param $requestBody array Defines request to be sent to API.
     * @return bool Defines order suitability to be sent to the API.
     */
    private function orderTotalValidation($requestBody): bool
    {
        $productCostSum = 0;
        foreach ($requestBody['products'] as $product)
        {
            $productCostSum += $product['totalPrice'];
        }

        return $requestBody['totalAmount'] === round($productCostSum, 2);
    }

    /**
     * Method checks that chosen loan product still exist in API system.
     * @param $loanProductType
     * @return bool
     */
    private function loanPaymentExistValidation($loanProductId): bool
    {
        // API Controller
        require_once DIR_SYSTEM .'library/holmbank/holmbank.php';
        $this->load->model('setting/setting');
        $mySettingValue = $this->model_setting_setting->getSetting('payment_holmbank');

        $this->load->model('extension/payment/holmbank');
        $holmbankRepository = $this->model_extension_payment_holmbank;
        $loanProductDb = $holmbankRepository->getMerchantProductById($loanProductId);

        $holmbank_api = new HolmbankApi($mySettingValue);
        $merchant_product_types = $holmbank_api->getLoanProducts();

        foreach ($merchant_product_types as $loanProductApi)
        {
            if ($loanProductApi['type'] === $loanProductDb['type'])
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Method generate unique key for payment transaction.
     * @return string unique value generated by e-store
     */
    private function generateUniqueKey($cart): string
    {
        $cart_id = !empty($cart->cart_id) ? $cart->cart_id : 0;

        // Just to be sure, that values never be the same add cart id to the unique key.
        return bin2hex(random_bytes(40)) . $cart_id;
    }

    /**
     * Method handles order status changes.
     * @param $status string Defines status that come from API.
     * @param $order_id int Defines order ID which status should be changed.
     * @return void Process status changes.
     */
    private function changeOrderStatus($status, $order_id): void
    {
        $this->load->model('setting/setting');
        $mySettingValue = $this->model_setting_setting->getSetting('payment_holmbank');

        $approvedStatus = empty($mySettingValue['payment_holmbank_accepted_order_status']) || $mySettingValue['payment_holmbank_accepted_order_status'] == "UNSELECTED"
            ? 'PROCESSING'
            : intval($mySettingValue['payment_holmbank_accepted_order_status']);

        $deniedStatus = empty($mySettingValue['payment_holmbank_denied_order_status']) || $mySettingValue['payment_holmbank_denied_order_status'] == "UNSELECTED"
            ? 'DENIED'
            : intval($mySettingValue['payment_holmbank_denied_order_status']);

        switch ($status)
        {
            case "APPROVED":
                $newState = $approvedStatus;
                break;
            case "PENDING":
                $newState = self::STATUS_AWAITING;
                break;
            case "REJECTED":
                $newState = $deniedStatus;
                break;
            default:
                return;
        }

        // Update the order status
        $this->load->model('checkout/order');
        $this->load->model('localisation/order_status');

        $order_statuses = $this->model_localisation_order_status->getOrderStatuses();

        $isSendMail = $newState == $approvedStatus;

        if (is_string($newState)) {
            foreach ($order_statuses as $order_status) {
                if (strtoupper($order_status['name']) == strtoupper($newState)) {
                    $this->model_checkout_order->addOrderHistory(
                        $order_id,
                        intval($order_status['order_status_id']),
                        'Holmbank payment (' . $newState .')',
                        $isSendMail
                    );
                    break;
                }
            }
        } else {
            foreach ($order_statuses as $order_status) {
                if (intval($order_status['order_status_id']) === $newState) {
                    $this->model_checkout_order->addOrderHistory(
                        $order_id,
                        $newState,
                        'Holmbank payment (' . $order_status['name'] . ')',
                        $isSendMail
                    );
                    break;
                }
            }
        }
    }
}