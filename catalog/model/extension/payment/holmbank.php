<?php

/**
 * Repository to process Database operations with Merchant Product Table.
 */
class ModelExtensionPaymentHolmbank extends Model {

    /**
     * Function is responsible for displaying payment methods.
     * @param array $address
     * @return array
     */
    public function getMethod($address, $total): array {
        $this->load->language('extension/payment/holmbank');

        $method_data = [];

        if ($total > 0)
        {
            $this->load->language('extension/payment/holmbank');

            $data['holmbank_options_title'] = $this->language->get('holmbank_options_title');
            $data['holmbank_style'] = 'catalog/view/theme/default/stylesheet/holmbank/payment.css';

            // Get Al Active Merchant Products Located in DB.
            $this->load->model('extension/payment/holmbank');
            $holmbankRepository = $this->model_extension_payment_holmbank;
            $activePaymentMethods = $holmbankRepository->getActiveMerchantProducts();

            $data['holmbank_payment_title'] = "Liisi järelmaks";
            $data['payment_options'] = array();
            foreach ($activePaymentMethods as $activePaymentMethod) {
                $data['payment_options'][$activePaymentMethod['id_merchant_product']] = [
                    'name' => $activePaymentMethod['name'],
                    'description' => $activePaymentMethod['description'],
                    'logo' => $activePaymentMethod['logo']
                ];
            }

            $payment_template = $this->load->view('extension/payment/holmbank', $data);

            $method_data = array(
                'code'       => 'holmbank',
                'title'      => $payment_template,
                'sort_order' => $this->config->get('payment_holmbank_sort_order'),
            );
        }

        return $method_data;
    }

    /**
     * Method gets all merchant products from Database that are currently active.
     * @return array Merchant products stored in database.
     */
        public function getActiveMerchantProducts()
    {
        $query = "SELECT * FROM " . DB_PREFIX . "holmbank_merchant_product WHERE is_enabled = true";
        return $this->db->query($query)->rows;
    }

    /**
     * Method gets all active for calculator merchant products from Database that are currently active.
     * @return array Merchant products stored in database.
     */
    public function getCalculatorMerchantProducts()
    {
        $query = "SELECT * FROM " . DB_PREFIX . "holmbank_merchant_product WHERE is_enabled = true AND is_calculator_displayed = true";
        return $this->db->query($query)->rows;
    }

    /**
     * Method process insertion of holmbank order in Database.
     * @param $orderData array Holmbank order data to be inserted in Database.
     * @return mixed boolean as indicator of successful operation.
     */
    public function addHolmbankOrder($orderData)
    {
        $query = $this->db->query(sprintf("INSERT INTO %sholmbank_orders 
            (id_shop, id_merchant_order, id_holmbank_order, x_payment_link_req_id) VALUES 
            ('%s','%s','%s','%s')", DB_PREFIX, (int) $orderData['id_shop'], (int) $orderData['id_merchant_order'],
            $this->db->escape($orderData['id_holmbank_order']), $this->db->escape($orderData['x_payment_link_req_id'])));
        return $query;
    }

    /**
     * Method process search among holmbank orders to find needed by holmbank order ID.
     * @param $orderId string Holmbank order ID.
     * @return mixed boolean as indicator of successful operation.
     */
    public function getHolmbankOrderById(int $orderId)
    {
        $query = 'SELECT * FROM `%1$sholmbank_orders` WHERE `id_holmbank_order`="%2$s"';
        return $this->db->query(sprintf($query, DB_PREFIX, (int) $orderId));
    }

    /**
     * Method gets merchant product from Database based on ID.
     * @param $id int Merchant product ID.
     */
    public function getMerchantProductById(int $id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "holmbank_merchant_product WHERE id_merchant_product = '$id'");
        return $query ? $query->row : $query;
    }

    /**
     * Method process search among holmbank orders to find needed by holmbank order ID and secret key.
     * @param $orderId string Holmbank order ID.
     * @param $uniqueKey string Transaction related secret key.
     * @return mixed boolean as indicator of successful operation.
     */
    public function getHolmbankOrderByIdAndKey($orderId, $uniqueKey)
    {
        $query = 'SELECT * FROM `%1$sholmbank_orders` WHERE `id_holmbank_order`="%2$s" AND `x_payment_link_req_id`="%3$s"';
        return $this->db->query(sprintf($query, DB_PREFIX, $orderId, $this->db->escape($uniqueKey)));
    }
}