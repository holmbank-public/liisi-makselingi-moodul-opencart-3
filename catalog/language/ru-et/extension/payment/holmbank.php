<?php
// Heading
$_['payment_method_logo'] = 'Логотип метода оплаты';
$_['estimation_text'] = 'Monthly payment from';
$_['holmbank_payment'] = 'Рассрочка Holmbank (изучите возможные варианты оплаты в следующем шаге)';
$_['holmbank_options_title'] = 'Варианты рассрочки Holmbank';
$_['payment_method_display_bo'] = "Оплачено через Holmbank";