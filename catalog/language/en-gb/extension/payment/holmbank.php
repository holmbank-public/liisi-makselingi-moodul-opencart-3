<?php
// Heading
$_['payment_method_logo'] = 'Payment method logo';
$_['estimation_text'] = 'Monthly payment from';
$_['holmbank_payment'] = 'Holmbank lease tax (explore possible payment options in the next step)';
$_['holmbank_options_title'] = 'Holmbank lease tax options';
$_['payment_method_display_bo'] = "Paid with Holmbank";