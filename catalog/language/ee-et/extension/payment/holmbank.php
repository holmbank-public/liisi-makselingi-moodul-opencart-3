<?php
// Heading
$_['payment_method_logo'] = 'Makse meetodi logo';
$_['estimation_text'] = 'Monthly payment from';
$_['holmbank_payment'] = 'Holmbank liisimakse (uurige järgmises etapis võimalikke maksevõimalusi)';
$_['holmbank_options_title'] = 'Holmbank liisimakse võimalused';
$_['payment_method_display_bo'] = "Makstud Holmbank'iga";